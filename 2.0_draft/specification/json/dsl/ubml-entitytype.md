## EntityType

实体类型是标准的结构化类型，实体类型代表了具体的实体，任何具有唯一标识的实体都可以用实体类型表述，比如客户，订单。
实体类型对象必须包含Kind成员，Kind的值为一个字符串。
实体类型对象必须包含Key成员，用于指定唯一标识属性。
实例类型对象可以包含多个属性成员。

```
"Product":{
  "kind": "EntityType",
  "Key": [
    "ID"
  ],
  "ID": "",
  "Code": ""
}
```


```
{
  "Version"： "0.1.0",
  "Model":{
    "Name": "SalesOrder",
	"Type": "BusinessEntity",
	"Header": {
      "Kind": "ComplexType",
	  "ID": "",
      "Code": "",
      "Name": "",
      "Namespace": "",
      "Type": "",
      "Language": "",
      "Translated": ,  
      "Extendable":   
    }
  }
}
这是描述了什么？能描述的东西是什么？
```

