## Property

属性是指结构化属性，属性可以是以下类型：

+ PrimitiveType
+ ComplexType
+ EnumerationType
+ 以上类型的集合

一个结构化属性，必须指定一个名称与类型。
属性名称在使用属性的结构化类型中，必须唯一。
在结构化类型中，包含多个属性成员，成员的名称就是属性名称，成员的值是一个对象。
属性对象可能包含以下成员：Nullable，DefaultValue（待补充，比如是decimal类型的话，需要指定长度与精度，Type，length，precision）

```
"Header": {
  "Kind": "ComplexType",
  "ID": "",
  "Code": "",
  "Name": "",
  "Namespace": "",
  "Type": "",
  "Language": "",
  "Translated": false,  
  "Extendable": true,  
}
```


