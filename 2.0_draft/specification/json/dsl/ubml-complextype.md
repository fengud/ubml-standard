## ComplexType

复杂类型是没有唯一标识的标准的结构化类型。由于复杂类型没有唯一标识，复杂类型不能脱离实体类型进行引用、创建、更新、删除等操作。复杂类型用来对属性进行分组。

复杂类型对象必须包含Kind成员，Kind的值为一个字符串。
复杂类型对象可以包含多个属性成员。

```
"Header": {
  "Kind": "ComplexType",
  "ID": "",
  "Code": "",
  "Name": "",
  "Namespace": "",
  "Type": "",
  "Language": "",
  "Translated": ,  
  "Extendable":   
}
```
