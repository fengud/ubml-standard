## ReturnType 返回类型

无返回值函数或有返回值函数<font color=red>重载</font>的返回类型可以是范围内的任何类型，或者范围内任何类型的集合。

操作返回必须包含类型属性，可以包含可空属性。

属性 类型（Type）
对于单值返回类型，Type的值是返回类型的限定名称。
对于具有集合值的返回类型，Type的值是集合名称的字符串。

属性 可空（Nullable）
Nullable的值是布尔值true或false。默认值为false。
对于单值返回类型，值true表示操作可以返回单个null值。 值false表示操作或函数将永远不会返回空值，并且如果无法计算结果，则将失败并返回错误响应。
对于具有集合值的返回类型，结果将始终是可以为空的集合。

```
<ReturnType Type="bdm.boolean"/>
```
```
<ReturnType Type="SalesOrder"/>
```
