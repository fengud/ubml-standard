## Function 有返回值函数
有返回值函数是定义服务的一种操作，不应该有可观测的效果，并且必须返回单个实例或任何类型的实例的集合。

有返回值函数名称是一个简单的标识符，在一份元模型结构描述中必须唯一。

有返回值函数必须指定返回类型，该返回类型必须是基本类型、实体类型或复杂类型，或者是基本类型、实体类型或复杂类型的集合。

有返回值函数可以定义在操作执行过程中使用的参数。

```
<Function Name="validation">
  <Parameter Name="salesOrderEntity" Type="SalesOrder"/>
  <Parameter Name="context" Type="BusinessContext"/>
  <ReturnType Type="bdm.boolean"/>
</Function>
```
```
<Function Name="Determination">
  <Parameter Name="salesOrderEntity" Type="SalesOrder"/>
  <Parameter Name="context" Type="BusinessContext"/>
  <ReturnType Type="SalesOrder"/>
</Function>
```