## Collection 集合

集合是一种元素，集合必须有名称，集合必须指明集合中元素的类型，一个集合中必须仅包含一种类型的0至多个元素。

```
<Collection Name="references" ElementType="MetaModel"/>
```


