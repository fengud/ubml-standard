## UBML XML 文档

UBML XML文档的根元素是Bdm，。Bdm元素必须包含一个Version属性，同时必须包含且仅包含一个模型元素。

```
<Bdm Version="0.1.0">
  <Model>
  ...
  </Model>
</Bdm>
```





