## Primitive Types
结构化类型由其他结构化类型与基础类型组成，UBML中定义以下基本类型：

|  类型   | 含义  |
|  ----  | ----  |
| bdm.Boolean  | 布尔型，其取值可能是：true：表示真；false：表示假；|
| bdm.Int  | 整数型 |
| bdm.String  | 字符串类型 |
| bdm.Date  | 日期型，不带时区信息 |
| bdm.DateTime  | 日期时间型，标准udt时间 |
| bdm.Decimal  | 数字型 |
| bdm.Binary  | 二进制类型，用于标识二进制数据 |


