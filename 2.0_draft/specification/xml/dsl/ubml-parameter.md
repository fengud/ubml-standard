## Parameter 参数

每个操作参数必须具有一个简单标识符的名称。操作参数编号在操作内必须唯一。
操作参数必须指定类型。 它可以是作用域中的任何类型，也可以是作用域中任何类型的集合。
操作参数可以包含可空属性。

<font face=黑体>属性 名称（Name）</font>
名称是有返回值函数（bdm: Function）的名称，名称的值必须以英文字母开头，可以包含数字，不能包含符号。

<font face=黑体>属性 类型（Type）</font>
对于单值返回类型，Type的值是返回类型的限定名称。
对于具有集合值的返回类型，Type的值是集合名称的字符串。

<font face=黑体>属性 可空（Nullable）</font>
Nullable的值是布尔值true或false。默认值为false。当值为true时，操作参数可以为空值。

```
<Parameter Name="salesOrderEntity" Type="SalesOrder"/>
<Parameter Name="context" Type="BusinessContext" Nullable=true/>
```
