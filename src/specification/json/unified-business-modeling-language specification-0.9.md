# 简介
UBML是描述工业管理应用业务模型的低代码开发语言，其可描述的模型范围包括UI、流程、领域服务、BFF、数据持久化等层级的模型，UBML使用JavaScript Object Notation (JSON)形式, 见[RFC8259]，提供了工业管理应用业务模型的全开发技术栈的统一描述。

## 术语
本文件中的关键词“必须”、“不得”、“必须”、“应”、“不应”、“建议”、“可”和“可选”应按照[RFC2119]中的描述进行解释。

UBML：Unified Business Modeling Language，统一业务建模语言。

BE：Business Entity，业务实体。

VO：View Model，视图模型。

DTO：Data Transfer Object，数据传输对象。

CMP：Component，构件。

EApi：External Api，外部服务。

## 规范性参考文献
[RFC2119] Bradner, S., “RFC中用于表示需求级别的关键词”, https://tools.ietf.org/html/rfc2119，1997年3月。
[RFC6570] Gregorio, J., Fielding, R., Hadley, M., Nottingham, M., and D. Orchard, “URI模板”, http://tools.ietf.org/html/rfc6570，2012年3月。
[RFC7493] Bray, T., Ed., "I-JSON消息格式" ,https://tools.ietf.org/html/rfc7493，2015年3月。
[RFC8259] Bray, T., Ed., “JavaScript对象表示法（JSON）数据交换格式”, http://tools.ietf.org/html/rfc8259，2017年12月。
[XML-Schema-2] W3C XML模式定义语言（XSD）1.1第2部分：数据类型, D. Peterson, S. Gao, C. M. Sperberg-McQueen, H. S. Thompson, P. V. Biron, A. Malhotra, http://www.w3.org/TR/2012/REC-xmlschema11-2-20120405/，2012年4月， 最新版本可见 http://www.w3.org/TR/xmlschema11-2/.

# 设计原则

# 业务实体（Business-Entity）

# 视图模型（View-Model）

# 外部服务（External-Api）
领域层的业务实体具有领域动作定义，包括内置CRUD操作以及自定义动作，它们可以通过视图模型和外部服务模型，发布为可由业务系统外部访问与调用的Web服务
在外部服务中，视图模型被视为服务的资源提供者（Resource Provider）；视图模型中的领域动作，是发生Web调用时被实际调用的对象，被视为服务的资源调用器（Resource Invoker）此外部服务具有标准兼容适配性，可被发布为多种业内主流风格的Web服务，比如Swagger/OpenAPI 3.0、WSDL、GraphQL等风格，易于适配和集成。

## 外部服务JSON定义
外部服务JSON对象 （External Api JSON Object）

其必须（MUST）包含的成员有：id,code,name,version,routeUrl,bizObject,criticalApplication,serviceUnit,service,models。
其可以（MAY）包含的成员有：description。

- id：外部服务的唯一标识，类型为string。
- code：外部服务的英文名称，类型为string。
- name：外部服务的中文名称，类型为string。
- version：外部服务的版本信息，类型为string。
- baseHttpPath：外部服务的基路径（URL）信息，是所有外部服务操作的公共URL段信息，类型为string。
- application:外部服务所属的应用，类型为string。应用代表一个包含一系列高内聚性的功能的业务系统，通常是一个完整业务系统中的子系统。
- serviceUnit:外部服务所属的微服务单元，微服务单元是一个完整的微服务模块，可以被独立部署和发布。一般一个关键应用包含一个或多个微服务单元。
- businessObject：外部服务所属的业务对象，类型为string。
- service：外部服务中包含的服务定义信息，类型为外部服务定义对象。
- models：外部服务中包含的服务数据模型信息，其类型是一个由外部服务数据模型对象组成的数组。
- description：外部服务的描述信息，类型为string。

## 外部服务定义
外部服务定义对象（Service Object）

其必须（MUST）包含的成员有：id,resourceId,resourceType,operations。
其可以（MAY）包含的成员有：resourceName，resourceCode。

- id：外部服务定义的唯一标识，类型为string。
- resourceType：外部服务定义所对应的资源提供者的类型，类型为string，值一般为VO（View Model）。
- resourceId：外部服务定义所对应的资源提供者的唯一标识，类型为string，一般为ViewModel的Id。
- operations：外部服务定义中包含的服务操作信息，其类型是一个由外部服务操作对象组成的数组。
- resourceName：外部服务定义所对应

## 外部服务操作定义
外部服务操作定义对象（Operation Object）

其必须（MUST）包含的成员有：id,code,name,httpMethod,httpPath,isDeprecated,resourceOperationId,parameters。
其可以（MAY）包含的成员有：description,isTransactional,extend1,extend2。

- id：外部服务操作的唯一标识，类型为string。
- code：外部服务操作的英文名称，类型为string。
- name：外部服务操作的中文名称，类型为string。
- httpMethod：外部服务操作对应的HTTP谓词动作，其类型为字符串类型的枚举，值可以为get、put、post、delete、patch、options、trace、head之一。
- httpPath：外部服务操作对应的URL路径模板，类型为string，与External Api Document Object中的routeUrl共同拼接成完整的URL。
- resourceOperationId：外部服务操作对应的Resource Provider（一般为View Model）中的操作唯一标识，类型为string。
- parameters：外部服务操作包含的参数信息，其类型是一个由外部服务参数对象组成的数组。
- description：外部服务操作的描述信息，类型为string。
- isTransactional：外部服务操作是否是事务性操作，类型为boolean，默认值为false。
- extend1：预留的外部服务操作扩展点1的信息，类型为string。
- extend2：预留的外部服务操作扩展点2的信息，类型为string。

## 外部服务参数定义
外部服务参数定义对象（Parameter Object）

其必须（MUST）包含的成员有：id,code,name,location,isPrimitiveType,isCollection,isReturnValue,isRequired。

其可以（MAY）包含的成员有：description,$ref,primitiveTypeKind,listDepth。

- id：外部服务参数的唯一标识，类型为string。
- code：外部服务参数的英文名称，类型为string。
- name：外部服务参数的中文名称，类型为string。
- location：外服务参数在HTTP请求中的位置（Location），其类型为字符串类型的枚举，值可以（MAY）为path、query、header、cookie之一，默认值为query。
- isPrimitiveType：外部服务参数的数据类型是否是基本类型，其类型为boolean，默认值为true。
- isCollection：外部服务参数的数据类型是否是集合类型，其类型为boolean，默认值为false。
- isReturnValue：外部服务参数是否为调用的返回值，其类型为boolean，默认值为false。
- isRequired，外部服务参数是否为必需参数，其类型为boolean，默认值为true。
- description：外部服务参数的描述信息，类型为string。
- $ref：当参数的数据类型为基本类型时，参数不得（MUST NOT）包含$ref；当参数的数据类型为结构化类型时，其数据类型通过引用的Model Object来表示，参数必须（MUST）包含$ref，其值为当前EApi中Model Object的唯一标识。
- primitiveTypeKind:当参数的数据类型为基本类型时，参数必须（MUST）包含primitiveTypeKind，其类型为字符串枚举，值可以（MAY）为string、boolean、int、double、byte、date、decimal之一；当参数的数据类型为结构化类型时，参数不得（MUST NOT）包含primitiveTypeKind。
- listDepth：外部服务参数的列表深度，类型为int，当isCollection为true时，参数必须（MUST）包含listDepth，且默认值为1；当isCollection为false时，参数不得（MUST NOT）包含listDepth。

## 外部服务数据模型定义
外部服务数据模型对象（Model Object）

其必须（MUST）包含的成员有：id,$ref。

- id：外部服务数据模型对象的唯一标识，类型为string。
- $ref：外部服务中结构化数据类型引用，代表其他模型中结构化数据类型的链接信息，类型为string。